const numbers = [20,16,6,76,45,10,9,64];

let bigger = numbers[0];

for (let i = 0; i < numbers.length; i++) {
	bigger = numbers[i] > bigger ? numbers[i] : bigger;
}

console.log(`bigger: ${bigger}`)
