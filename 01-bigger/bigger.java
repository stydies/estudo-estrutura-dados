class Bigger {
    public static void main(String [] args) {
        int [] numbers = {1,2,15,6,7,37};
        int bigger = numbers[0];

        for (int number : numbers) {
            bigger = number > bigger ? number : bigger;
        }
        System.out.println("bigger: " + bigger);
    }
}