#include <iostream>
using namespace std;

int numbers[5] = {1,2,7,3,15};
int bigger = numbers[0];

int main() {
  for (int i =0; i < 5; ++i) {
    bigger = numbers[i] > bigger ? numbers[i]: bigger;
	}
  
  cout << "bigger: " << bigger;
  return 0;
}